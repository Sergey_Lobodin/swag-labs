Hello! 
This is my project - **SWAG LABS**.
 
 **SWAG LABS** is a one-page web-shop site, which consist of: 
1.  Authorization.
1.  Catalog of products.
1.  Card of product.
1.  Cart.
1.  Menu.
1.  Filters.
1.  Social networks links.

 And this web-site access you to place and sell your products like a seller, but in role of customer you can look, choose and add to the cart some products and order them to yourself.

**SWAG LABS URL:** https://www.saucedemo.com/

I've done my reseach of this web-site and I was using boxes of testing (Test documentation, Check-lists and Test-cases), which you can see and review at the links below.

**Google docs (Test Plan):** https://docs.google.com/document/d/1rBIlPU1YEhECfI0-bGG7QmJTOQsu0DgngyiS6qLv9F0/edit?usp=sharing

**Google docs (Check-List):** https://docs.google.com/spreadsheets/d/1kn5FCMxHNILnxY3tPScpQA8prsIOJlDEWRxZywLKmHQ/edit?usp=sharing

**Google docs (Test-Cases):** https://docs.google.com/spreadsheets/d/1ZV_nIpvMQiP16Jr62S-8QRP3h1cjhlKedK3Tj7mge0I/edit?usp=sharing

**Google docs (Bug-List):** https://docs.google.com/spreadsheets/d/1HIDYptuB-x5i4q-BH9DUpb4Gc_vxn_1WGtixI969agE/edit?usp=sharing

